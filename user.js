user_pref("browser.tabs.hoverPreview.showThumbnails", false);

user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);

user_pref("browser.startup.homepage", "chrome://browser/content/blanktab.html");
user_pref("browser.newtabpage.enabled", false);

user_pref("browser.search.suggest.enabled", false);

user_pref("browser.urlbar.suggest.bookmark", false);
user_pref("browser.urlbar.suggest.clipboard", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("browser.urlbar.suggest.topsites", false);
user_pref("browser.urlbar.suggest.engines", false);

user_pref("browser.contentblocking.category", "strict");

user_pref("signon.rememberSignons", false);

user_pref("privacy.history.custom", true);
user_pref("places.history.enabled", false);
user_pref("browser.formfill.enable", false);
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
user_pref("privacy.clearOnShutdown_v2.siteSettings", true);

user_pref("datareporting.healthreport.uploadEnabled", false);

user_pref("dom.security.https_only_mode", true);

user_pref("extensions.pocket.enabled", false);
user_pref("identity.fxaccounts.enabled", false);
